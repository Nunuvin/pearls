#!/usr/bin/perl
use strict;
use warnings;
use 5.10.0;
#find regex in files, print matching lines in format
#filename lineNumber line

scalar @ARGV == 1 or @ARGV == 2
    or die "Usuage: perl $0 <regex> [-r][--recursive]";

my $dir = ".";
sub processDir {
    opendir(DIR, $_[0]) or die $!;

    my @files;
    while( my $f = readdir(DIR)){
        next unless (-f "$_[0]/$f");
        push @files, $f;
    }
    #say join ' ', @files;

    closedir(DIR);

    for my $f (@files){
        #say $_[0];
        #say $f;
        open(my $fh, "<", $_[0].'/'.$f)
            or die "failed to open $f";
        my @lines = <$fh>;
        foreach (0 ..$#lines){
            if($lines[$_] =~  /$ARGV[0]/){
                say $f." ".($_+1)." ".$lines[$_];
            }
        
        }
        close($fh);

    }
}

sub descend {

    opendir(DIR, $_[0]) or die $!;
    my $curDir = $_[0];
    my @dirs;
    while( my $d = readdir(DIR)){
        next unless (-d "$_[0]/$d");
        if($d eq "." || $d eq ".."){ next;}
        push @dirs, $d;
    }
    #say join ' ', @files;

    closedir(DIR);
    foreach (@dirs){
        my $nextDir = $curDir.'/'.$_;
        #say $curDir.' '.$nextDir;
        #processDir ($curDir);
        descend ($nextDir);
    }
        processDir ($curDir);
}

if ($ARGV[1] eq "-r" or $ARGV[1] eq "--recursive"){
    descend $dir;
}else{
    processDir $dir;
}
